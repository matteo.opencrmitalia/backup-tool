#!/bin/bash

# Script backup opencrmitalia...
# Stesura iniziale Matteo Baranzoni...
# Rivisto da Pancio 08/03/2021...



# Inizio definizione variabili
# ****************************

LOGPATH=/home/ubuntu/log
CONFFILE=/home/ubuntu/scripts/backupconfig

# Fine definizione variabili...
# *****************************



source $CONFFILE
EMAIL="$email" >&2	
USERNAME="$user" >&2
PW="$password" >&2

# Definizione variabili
TIMESTAMP=$(date +"%F") #data
LOGFILE=$LOGPATH/clean.log


echo "Data:" $TIMESTAMP > $LOGFILE
echo " " >>$LOGFILE

for D in /home/*/public_html; do
	PERCORSO=${D} #percorso del crm

	[ ! -d "$PERCORSO" ] && continue
	[ ! -e "$PERCORSO/config.inc.php" ] && continue

	DB=$(/usr/bin/php -r "include('${PERCORSO}/config.inc.php');echo \$dbconfig['db_name'];") #nome db
	URL=$(/usr/bin/php -r "include('${PERCORSO}/config.inc.php');echo \$site_URL;") #SiteURL

  # query sql
	mysql -u $USERNAME -p$PW $DB -e "delete from adodb_logsql where timestamp<DATE_ADD(NOW(), INTERVAL -10 DAY)" >> $LOGFILE
	mysql -u $USERNAME -p$PW $DB -e "delete from vtiger_wf_log where timestamp<DATE_ADD(NOW(), INTERVAL -10 DAY)" >> $LOGFILE
	mysql -u $USERNAME -p$PW $DB -e "delete from vtiger_wf_logtbl where date<DATE_ADD(NOW(), INTERVAL -10 DAY)" >> $LOGFILE
	mysqlcheck -Aos $DB


  # cancellazione file
  [ ! -e "$PERCORSO/info.php" ] || rm ${PERCORSO}/info.php >> $LOGFILE
  [ ! -e "$PERCORSO/phpinfo.php" ] || rm ${PERCORSO}/phpinfo.php >> $LOGFILE
  [ ! -e "$PERCORSO/adminer.php" ] || rm ${PERCORSO}/adminer.php >> $LOGFILE
  [ ! -d "$PERCORSO/oci" ] || rm ${PERCORSO}/oci -R >> $LOGFILE
  [ ! -d "$PERCORSO/pma" ] || rm ${PERCORSO}/pma -R >> $LOGFILE
  [ ! -d "$PERCORSO/pkg" ] || rm ${PERCORSO}/pkg -R >> $LOGFILE
  [ ! -d "$PERCORSO/packages" ] || rm ${PERCORSO}/packages -R >> $LOGFILE
  [ ! -d "$PERCORSO/migrate" ] || rm ${PERCORSO}/migrate -R >> $LOGFILE
  [ ! -d "$PERCORSO/phpmyadmin" ] || rm ${PERCORSO}/phpmyadmin -R >> $LOGFILE


  echo "-------------- " >>$LOGFILE
  echo "CRM:" $DB >> $LOGFILE
  echo "dimensione httdocs" >> $LOGFILE
  du -sh $PERCORSO >> $LOGFILE
  echo "dimensione DB" >> $LOGFILE
  du /var/lib/mysql/$DB -sh  >> $LOGFILE
  echo " " >>$LOGFILE
  echo " " >>$LOGFILE
done



echo " " >>$LOGFILE
echo " " >>$LOGFILE

echo "Dimensioni tabelle più grandi nei db:" >> $LOGFILE
find /var/lib/mysql -type f -exec du -Sh {} + | sort -rh | head -n 20 >>$LOGFILE



echo " " >>$LOGFILE
echo " " >>$LOGFILE
echo "File zip:" >> $LOGFILE
find /home/ -iname \*.zip  -not -path "/home/*/public_html/test/*"  -not -path "/home/*/public_html/storage/*" -not -path "/home/*/public_html/libraries/tcpdf/fonts/*" -exec du -Sh {} +| sort -rh | head -n 20 >>$LOGFILE 
find /home/ -iname \*.tar.gz  -not -path "/home/*/public_html/test/*"  -not -path "/home/*/public_html/storage/*" -not -path "/home/*/public_html/libraries/tcpdf/fonts/*" -exec du -Sh {} +| sort -rh | head -n 20 >>$LOGFILE


echo " " >>$LOGFILE
echo " " >>$LOGFILE
echo "File SQL:" >> $LOGFILE
find /home/ -iname \*.sql -exec du -Sh {} +| sort -rh | head -n 20 >>$LOGFILE

echo "EMAIL=$EMAIL"
sendmail -F "server@opencrmitalia.com" $EMAIL < $LOGFILE

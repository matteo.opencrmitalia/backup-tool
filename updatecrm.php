<?php
	if (PHP_SAPI != "cli") {
		die("Use in only CLI");
	}

	if ($argc < 2) {
		die("updarecrm.php <cmrurl> <logfile>");
	}

	$domain = $argv[1];
	$domain = str_replace(["https://", "http://"], ['', ''], $domain);
	$domain = trim($domain, '/');

	$file = $argv[2];
	
	if (!file_exists($file)) {
    	return $file;
		die("File doesn't exist"); // No log file found
	}

	function sendRequest($url, $params = [], $method = "GET")
	{
		$dataquerystring = http_build_query($params);

		$url = rtrim($url, "/");
		$url .= "/webservice.php";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if ($method == "POST") {
			curl_setopt($ch, CURLOPT_POST, count($params));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $dataquerystring);
		}
		else {
			$url .= "?" . $dataquerystring;
		}

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
		$result = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		$jresult = json_decode($result, true);
		if (is_array($jresult) && !empty($jresult) && isset($jresult["success"]) && $jresult["success"]) {
			return ($jresult["result"]);
		} else {
			// print_r(["error" => $jresult["error"]["message"] ?? "Errore"]);
			return false;
		}
	}

	$crmurl = "https://crm.opencrmitalia.com";

	$username = "Backup";
	$useracceskey = "0jrAUuJvPTZAa7tN";

	// Login
	$params = ["operation" => "getchallenge", "username" => $username];
	$challenge = sendRequest($crmurl, $params);

	$generatedKey = md5($challenge["token"] . $useracceskey);
	$params = ["operation" => "login", "username" => $username, "accessKey" => $generatedKey];
	$login = sendRequest($crmurl, $params, "POST");
	$sessionName = $login["sessionName"];
	//End Login

	$query = "SELECT id FROM Serviziocloud WHERE serviziocloud_tks_dominio = '$domain' LIMIT 0, 1;";
	$params = ["operation" => "query", "sessionName" => $sessionName, "query" => $query];
	$resultQuery = sendRequest($crmurl, $params, "GET");

	if (empty($resultQuery)) {
		die("CRM not Found"); // Not Found
	}
	$resultQuery = $resultQuery[0];

	$todayDate = date("Y-m-d");
	$element = [
		"id" => $resultQuery["id"],
		// "cf_2048" => $todayDate, // Data Ultimo Backup DB
		// "cf_2050" => $todayDate, // Data Ultimo Backup htdocs
		// "data_ultimo_backup_storage" => $todayDate, // Data Ultimo Backup Storage
		"backup_log" => file_get_contents($file),
	];

	$params = ["operation" => "revise", "sessionName" => $sessionName, "element" => json_encode($element)];
	$result = sendRequest($crmurl, $params, "POST");
	//$result = true;

	if ($result) {
		echo "CRM Updated Successfully Service N.".$result['serviziocloudno'];
	}
	else {
		echo "CRM Update Failed";
	}
?>


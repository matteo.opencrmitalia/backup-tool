#!/bin/bash

# Script backup opencrmitalia...
# Stesura iniziale Matteo Baranzoni...
# Rivisto da Pancio 08/03/2021...



# Inizio definizione variabili
# ****************************

TMPPATH=/home/ubuntu
BASEPATH=/home/ubuntu/scripts
LOGPATH=/home/ubuntu/log
CONFFILE=/home/ubuntu/scripts/backupconfig

# Fine definizione variabili...
# *****************************




source $CONFFILE
AMAZON="$server" >&2
DBUSR="$user" >&2
DBPW="$password" >&2

TIMESTAMP=$(date +"%F")
GIORNO=$(date +%A)


rm $LOGPATH/$GIORNO
mkdir $LOGPATH/$GIORNO

for D in /home/*/public_html; do
	PERCORSO=${D}

	[ ! -d "$PERCORSO" ] && continue
	[ ! -e "$PERCORSO/config.inc.php" ] && continue

	DB=$(/usr/bin/php -r "include('${PERCORSO}/config.inc.php');echo \$dbconfig['db_name'];")
	URL=$(/usr/bin/php -r "include('${PERCORSO}/config.inc.php');echo \$site_URL;")

	LOGFILE=$LOGPATH/$GIORNO/${DB}.log
	
	echo "CRM:" $DB > $LOGFILE
	echo "Data:" $TIMESTAMP >> $LOGFILE
	echo "Percorso:" $PERCORSO >> $LOGFILE
	echo "SiteURL:" $URL >> $LOGFILE



	# ********
	# DUMP SQL
	echo "" >> $LOGFILE
	echo "**** mysql" >> $LOGFILE

	mysqldump -al -u $DBUSR -p$DBPW $DB > $TMPPATH/$DB.sql 2>> $LOGFILE

	echo "" >> $LOGFILE
	echo "dimensione file sql: " >> $LOGFILE && du $TMPPATH/$DB.sql -h >> $LOGFILE

	/usr/bin/zip $TMPPATH/$DB.sql.zip $TMPPATH/$DB.sql

	echo "dimensione file zip sql: " >> $LOGFILE && du $TMPPATH/$DB.sql.zip -h >> $LOGFILE

	##INVIO SQL su amazon
	echo "" >> $LOGFILE
	echo "**** Carica DB su Amazon" >> $LOGFILE

	s3cmd -v put $TMPPATH/$DB.sql.zip $AMAZON/$GIORNO/ 2>> $LOGFILE

	echo "**** fine caricamento DB" >> $LOGFILE
	echo "" >> $LOGFILE

	rm $TMPPATH/$DB.sql.zip $TMPPATH/$DB.sql


	
	# **********
	# backup WWW
	echo "" >> $LOGFILE
	echo "**** Carica CRM su Amazon" >> $LOGFILE

	/usr/bin/zip -r $TMPPATH/$DB.www.zip $PERCORSO -x "$PERCORSO/storage/*" "$PERCORSO/.git/*" "$PERCORSO/oci/*" "$PERCORSO/packages/*"

	echo "dimensione file www: " >> $LOGFILE && du $TMPPATH/$DB.www.zip -h >> $LOGFILE

	s3cmd -v put $TMPPATH/$DB.www.zip $AMAZON/$GIORNO/ 2>> $LOGFILE

	echo "**** fine caricamento CRM" >> $LOGFILE
	echo "" >> $LOGFILE

	rm $TMPPATH/$DB.www.zip


	# **************
	# sync storage
	echo "" >> $LOGFILE
	echo "**** Carica storage su Amazon" >> $LOGFILE

	s3cmd sync  $PERCORSO/storage  $AMAZON/storage/$DB/ 2>> $LOGFILE

	echo "**** fine caricamento DB" >> $LOGFILE
	echo "" >> $LOGFILE
	
		


	# ************
	# Aggiorno CRM
	sudo chmod 777 $LOGFILE  >> $LOGFILE
	/usr/bin/php -f $BASEPATH/updatecrm.php "${URL}" "${LOGFILE}" >> $LOGFILE
done
